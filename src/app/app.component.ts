import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ViewChild
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnDestroy {
  title = 'html5-vid-branching';

  introFile;
  manateeChoiceFile = '../assets/manatee.webm';
  ParrotChoiceFile;
  manateeFartFile;
  parrotFartFile;
  endFile;

  mediaSource: MediaSource;
  sourceBuffer: SourceBuffer;

  NUM_CHUNKS = 1;
  chunkSize: number;
  file: Blob;

  mimeCodec = 'video/webm; codecs="vorbis,vp8"';

  @ViewChild('videoPlayer') videoPlayer: any;

  constructor() {}

  ngOnInit(): void {
    this.mediaSource = new MediaSource();

    // Check that browser has support for media codec
    console.log(MediaSource.isTypeSupported(this.mimeCodec));

    this.mediaSource.addEventListener('sourceopen', this.callback);

    this.videoPlayer.src = window.URL.createObjectURL(this.mediaSource);
  }

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {}

  callback(e) {
    console.log('yeet');
    this.sourceBuffer = this.mediaSource.addSourceBuffer(
      'video/webm; codecs="vorbis,vp8"'
    );

    // console.log('mediaSource readyState: ' + this.readyState);

    this.GET(this.manateeChoiceFile, uInt8Array => {
      this.file = new Blob([uInt8Array], { type: 'video/webm' });
      this.chunkSize = Math.ceil(this.file.size / this.NUM_CHUNKS);

      console.log('num chunks:' + this.NUM_CHUNKS);
      console.log(
        'chunkSize:' + this.chunkSize + ', totalSize:' + this.file.size
      );

      // Slice the video into NUM_CHUNKS and append each to the media element.
      const i = 0;

      this.readChunk_(i); // Start the recursive call by self calling.
    });
  }

  GET(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    xhr.send();

    xhr.onload = e => {
      if (xhr.status !== 200) {
        alert('Unexpected status code ' + xhr.status + ' for ' + url);
        return false;
      }
      callback(new Uint8Array(xhr.response));
    };
  }

  readChunk_(i) {
    const reader = new FileReader();

    // Reads aren't guaranteed to finish in the same order they're started in,
    // so we need to read + append the next chunk after the previous reader
    // is done (onload is fired).
    reader.onload = (e: ProgressEvent) => {
      try {
        const arr = new Uint8Array(reader.result as ArrayBuffer).subarray(0, 4);
        this.sourceBuffer.appendBuffer(arr);
        console.log('appending chunk:' + i);
      } catch (e) {
        console.log(e);
      }

      if (i === this.NUM_CHUNKS - 1) {
        if (!this.sourceBuffer.updating) {
          this.mediaSource.endOfStream();
        }
      } else {
        if (this.videoPlayer.paused) {
          this.videoPlayer.play(); // Start playing after 1st chunk is appended.
        }

        this.sourceBuffer.addEventListener('updateend', ev => {
          if (i < this.NUM_CHUNKS - 1) {
            this.readChunk_(++i);
          }
        });
      }
    };

    const startByte = this.chunkSize * i;
    const chunk = this.file.slice(startByte, startByte + this.chunkSize);

    reader.readAsArrayBuffer(chunk);
  }
}
